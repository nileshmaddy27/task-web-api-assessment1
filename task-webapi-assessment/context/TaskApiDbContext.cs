﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using TaskApi.Models;


namespace TaskAPI.Context
{
    public class TaskApiDbContext : DbContext
    {
        public TaskApiDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Tasks> Tasks { get; set; }

        public DbSet<SubTask> SubTasks { get; set; }
        public DbSet<User> Users { get; set; }
    }
}