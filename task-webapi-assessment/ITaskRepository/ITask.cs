﻿using TaskApi.Models;


namespace TaskAPI.ITaskRepository
{
    public interface ITask
    {
        TaskApi.Models.Tasks AddTask(TaskApi.Models.Tasks task);
        IEnumerable<TaskApi.Models.Tasks> GetAllTasks();
        TaskApi.Models.Tasks GetTaskById(int id);
        void EditTask(int id, TaskApi.Models.Tasks task);
        void DeleteTask(int id);
    }



}