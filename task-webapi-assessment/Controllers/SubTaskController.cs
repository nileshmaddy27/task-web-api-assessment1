﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskApi.Models;
using TaskAPI.Context;
using TaskAPI.ITaskRepository;


[ApiController]
[Route("api/subtasks")]
[Authorize]
public class SubTaskController : ControllerBase
{
    ISubTask _repo;

    public SubTaskController(ISubTask repo)
    {
        _repo = repo;
    }

    [HttpPost]
    public ActionResult<SubTask> AddSubTask(SubTask subTask)
    {
        _repo.AddSubTask(subTask);
        return Ok(subTask);
    }

    [HttpGet("{taskId}")]
    public ActionResult<IEnumerable<SubTask>> GetSubTasksForTask(int taskId)
    {
        var subTasksForTask = _repo.GetSubTasksForTask(taskId);
        return Ok(subTasksForTask);
    }
}