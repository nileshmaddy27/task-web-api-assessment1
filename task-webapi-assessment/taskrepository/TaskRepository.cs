﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskApi.Models;
using TaskAPI.Context;
using TaskAPI.ITaskRepository;


namespace TaskAPI.TaskRepository
{
    public class TaskRepository : ITask
    {
        private readonly TaskApiDbContext _dbContext;

        public TaskRepository(TaskApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public TaskApi.Models.Tasks AddTask(TaskApi.Models.Tasks task)
        {
            task.CreatedAt = DateTime.Now;
            _dbContext.Tasks.Add(task);
            _dbContext.SaveChanges();
            return task;
        }

        [HttpGet]
        public IEnumerable<TaskApi.Models.Tasks> GetAllTasks()
        {
            var tasks = _dbContext.Tasks.ToList();
            return (IEnumerable<TaskApi.Models.Tasks>)tasks;
        }

        [HttpGet("{id}")]
        public TaskApi.Models.Tasks GetTaskById(int id)
        {
            var task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);
            return task;
        }

        [HttpPut("{id}")]
        public void EditTask(int id, TaskApi.Models.Tasks updatedTask)
        {
            var task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);

            task.Name = updatedTask.Name;
            task.CreatedBy = updatedTask.CreatedBy;
            task.Description = updatedTask.Description;

            _dbContext.SaveChanges();
        }

        [HttpDelete("{id}")]
        public void DeleteTask(int id)
        {
            var task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);
            _dbContext.Tasks.Remove(task);
            _dbContext.SaveChanges();

        }
    }

}