﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskApi.Models;
using TaskAPI.Context;
using TaskAPI.ITaskRepository;


namespace TaskAPI.TaskRepository
{
    public class SubTaskRepository : ISubTask
    {
        private readonly TaskApiDbContext _dbContext;

        public SubTaskRepository(TaskApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public SubTask AddSubTask(SubTask subTask)
        {
            subTask.CreatedAt = DateTime.Now;
            _dbContext.SubTasks.Add(subTask);
            _dbContext.SaveChanges();
            return subTask;
        }

        [HttpGet("{taskId}")]
        public IEnumerable<SubTask> GetSubTasksForTask(int taskId)
        {
            var subTasksForTask = _dbContext.SubTasks.Where(st => st.TaskId == taskId).ToList();
            return subTasksForTask;
        }
    }
}